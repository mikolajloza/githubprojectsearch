package com.example.mikolaj.githubprojectsearch.view;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mikolaj.githubprojectsearch.R;

/**
 * Created by Mikołaj on 2017-01-27.
 */

public class ProjectViewHolder extends RecyclerView.ViewHolder {
    public final ImageView avtarImageView;
    public final ImageView starImageView;
    public final TextView nameOfProjectTextView;
    public final TextView countOfStrasTextView;

    public ProjectViewHolder(View itemView) {
        super(itemView);
        this.avtarImageView = (ImageView) itemView.findViewById(R.id.avatar_image_view);
        this.starImageView = (ImageView) itemView.findViewById(R.id.star_image_view);
        this.nameOfProjectTextView = (TextView) itemView.findViewById(R.id.project_name_text_view);
        this.countOfStrasTextView = (TextView) itemView.findViewById(R.id.count_of_stars_text_view);

    }
}
