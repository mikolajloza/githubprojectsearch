package com.example.mikolaj.githubprojectsearch.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Mikołaj on 2017-01-29.
 */

public class ProjectOwner {

    @SerializedName("avatar_url")
    private final String avatarUrl;

    public ProjectOwner(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }
}

