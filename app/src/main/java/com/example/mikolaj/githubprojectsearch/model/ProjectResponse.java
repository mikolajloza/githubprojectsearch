package com.example.mikolaj.githubprojectsearch.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Mikołaj on 2017-01-27.
 */

public class ProjectResponse {
@SerializedName("items")
    private final List<Project>  projects;

    public ProjectResponse(List<Project> projects) {
        this.projects = projects;
    }

    public List<Project> getProjects() {
        return projects;
    }
}
