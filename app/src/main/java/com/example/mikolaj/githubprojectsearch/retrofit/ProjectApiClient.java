package com.example.mikolaj.githubprojectsearch.retrofit;

import com.example.mikolaj.githubprojectsearch.model.ProjectResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Mikołaj on 2017-01-27.
 */

public interface ProjectApiClient {


    @GET("search/repositories")
    Call<ProjectResponse> getProjects(
            @Query("q") String query,
            @Query("sort") String sort
    );
}