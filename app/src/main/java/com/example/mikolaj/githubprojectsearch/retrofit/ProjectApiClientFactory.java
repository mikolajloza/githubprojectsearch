package com.example.mikolaj.githubprojectsearch.retrofit;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Mikołaj on 2017-01-27.
 */

public class ProjectApiClientFactory {

    private static final String BASE_URL="https://api.github.com/" ;

    public ProjectApiClient createProjectApiClient() {
        Retrofit retrofit= new Retrofit.Builder()
                .baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create())
                .build();


        return retrofit.create(ProjectApiClient.class);
    }

}
