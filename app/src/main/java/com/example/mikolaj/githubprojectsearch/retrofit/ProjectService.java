package com.example.mikolaj.githubprojectsearch.retrofit;

import com.example.mikolaj.githubprojectsearch.model.Project;
import com.example.mikolaj.githubprojectsearch.model.ProjectResponse;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;


/**
 * Created by Mikołaj on 2017-01-27.
 */

public class ProjectService {

    private final ProjectApiClient projectApiClient;
    private final String QUERY_PARAM = "language:Kotlin";
    private final String SORT_PARAM = "starsorder:desc";
    private String queryParam;


    public ProjectService() {
        projectApiClient = new ProjectApiClientFactory().createProjectApiClient();
    }

    public List<Project> getProjects(String param) throws IOException {
        if (param.equals("") || param.isEmpty()) {
            queryParam = QUERY_PARAM;
        } else {
            queryParam = param;
        }

        Call<ProjectResponse> call = projectApiClient.getProjects(queryParam, SORT_PARAM);
        Response<ProjectResponse> projectResponse = call.execute();

        return projectResponse.body().getProjects();
    }


}
