package com.example.mikolaj.githubprojectsearch.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Mikołaj on 2017-01-27.
 */

public class Project {
    @SerializedName("name")
    private final String nameOfProject;

    @SerializedName("owner")
    private final ProjectOwner projectOwners;

    @SerializedName("stargazers_count")
    private final int countOfStars;

    public Project(String nameOfProject, ProjectOwner projectOwners, int countOfStars) {
        this.nameOfProject = nameOfProject;
        this.projectOwners = projectOwners;
        this.countOfStars = countOfStars;
    }

    public String getNameOfProject() {
        return nameOfProject;
    }

    public ProjectOwner getProjectOwners() {
        return projectOwners;
    }

    public int getCountOfStars() {
        return countOfStars;
    }
}