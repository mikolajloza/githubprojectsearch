package com.example.mikolaj.githubprojectsearch.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mikolaj.githubprojectsearch.model.ListProject;
import com.example.mikolaj.githubprojectsearch.R;
import com.example.mikolaj.githubprojectsearch.model.Project;
import com.example.mikolaj.githubprojectsearch.model.ProjectOwner;
import com.squareup.picasso.Picasso;

import java.util.List;

import jp.wasabeef.picasso.transformations.RoundedCornersTransformation;

/**
 * Created by Mikołaj on 2017-01-27.
 */

public class ProjectRecyclerViewAdapter extends RecyclerView.Adapter<ProjectViewHolder> {
    private final Context context;
    private final LayoutInflater layoutInflater;
    private List<ListProject> items;
    private final int radius = 30;
    private final int margin = 0;


    public ProjectRecyclerViewAdapter(Context context, List<ListProject> items) {
        this.context = context;
        this.items = items;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public ProjectViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.view_projects_item, parent, false);

        return new ProjectViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ProjectViewHolder holder, int position) {

        ProjectViewHolder projectViewHolder =  holder;
        Project project = (Project) items.get(position).getData();
        ProjectOwner projectOwner = project.getProjectOwners();

        projectViewHolder.nameOfProjectTextView.setText(project.getNameOfProject());
        projectViewHolder.countOfStrasTextView.setText(String.valueOf(project.getCountOfStars()));

        if (projectOwner.getAvatarUrl() == null) {
            setImage(String.valueOf(R.mipmap.no_image),projectViewHolder);
            return;
        }
        setImage(projectOwner.getAvatarUrl(),projectViewHolder);
    }

    private void setImage(String imageResource ,ProjectViewHolder projectViewHolder) {
        Picasso.with(context)
                .load(imageResource)
                .fit().centerCrop()
                .transform(new RoundedCornersTransformation(radius, margin))
                .into(projectViewHolder
                        .avtarImageView);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}
