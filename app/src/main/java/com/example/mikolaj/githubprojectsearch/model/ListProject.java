package com.example.mikolaj.githubprojectsearch.model;

/**
 * Created by Mikołaj on 2017-01-27.
 */

public class ListProject<Project> {
    private final Project data;

    protected ListProject(Project data) {
        this.data = data;
    }

    public Project getData() {
        return data;
    }


}
