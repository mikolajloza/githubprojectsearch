package com.example.mikolaj.githubprojectsearch;

import android.os.AsyncTask;

import com.example.mikolaj.githubprojectsearch.model.Project;
import com.example.mikolaj.githubprojectsearch.retrofit.ProjectService;

import java.util.List;

/**
 * Created by Mikołaj on 2017-01-27.
 */

public class GetProjectAsyncTask extends AsyncTask<String, Void, GetProjectAsyncTask.ResultData> {
    private final ProjectService projectService;
    private final ProjectsDownloadedListener projectsDownloadedListener;

    public GetProjectAsyncTask(ProjectsDownloadedListener projectsDownloadedListener) {
        this.projectsDownloadedListener = projectsDownloadedListener;
        projectService = new ProjectService();
    }

    @Override
    protected ResultData doInBackground(String... strings) {

        try {

            List<Project> projects = projectService.getProjects(strings[0]);

            return new ResultData(projects);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(ResultData data) {
        projectsDownloadedListener.onProjectsDownloaded(data);
    }

    public interface ProjectsDownloadedListener {

        void onProjectsDownloaded(ResultData data);
    }

    public static class ResultData {
        private final List<Project> projects;

        public ResultData(List<Project> projects) {
            this.projects = projects;
        }

        public List<Project> getProjects() {
            return projects;
        }
    }

}
