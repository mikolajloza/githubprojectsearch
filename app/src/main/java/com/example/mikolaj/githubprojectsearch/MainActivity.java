package com.example.mikolaj.githubprojectsearch;

import android.content.Context;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.mikolaj.githubprojectsearch.model.ListProject;
import com.example.mikolaj.githubprojectsearch.model.Project;
import com.example.mikolaj.githubprojectsearch.view.ProjectRecyclerViewAdapter;

import java.util.LinkedList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements GetProjectAsyncTask.ProjectsDownloadedListener {
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private EditText searchProjectEditText;
    private Button searchProjectButton;
    private String query = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.activity_main_swipe_refresh_layout);

        searchProjectEditText = (EditText) findViewById(R.id.search_project_edit_text);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        searchProjectButton = (Button) findViewById(R.id.search_project_button);

        recyclerView = (RecyclerView) findViewById(R.id.projects_recycler_view);
        new GetProjectAsyncTask(this).execute(query);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                refreshProjects();

            }
        });
        searchProjectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!searchProjectEditText.getText().toString().equals("")) {
                    onSearchProject();
                }else {
                    refreshOnClick();
                }

            }
        });
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onProjectsDownloaded(GetProjectAsyncTask.ResultData data) {
        swipeRefreshLayout.setRefreshing(false);

        if (data == null) {
            String message = " Something goes wrong! ";
            Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            return;
        }
        if (data.getProjects().isEmpty()) {
            String message = "Not found : " + query;
            Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            return;
        }

        List<Project> projects = data.getProjects();
        List<ListProject> items = listItems(projects);

        ProjectRecyclerViewAdapter adapter = new ProjectRecyclerViewAdapter(this, items);
        recyclerView.setAdapter(adapter);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

    }

    private List<ListProject> listItems(List<Project> projects) {

        List<ListProject> items = new LinkedList<>();
        for (Project project : projects) {
            items.add(new ListProject(project));
        }
        return items;
    }

    private void refreshProjects() {
        MediaPlayer mediaPlayer = MediaPlayer.create(this, R.raw.sound_refresh);
        mediaPlayer.start();
        new GetProjectAsyncTask(MainActivity.this).execute(query);
        searchProjectEditText.setText("");
        hideKeyboard();
    }

    private void onSearchProject() {
        query = searchProjectEditText.getText().toString();
        new GetProjectAsyncTask(MainActivity.this).execute(query);
        swipeRefreshLayout.setRefreshing(true);
        searchProjectEditText.setText("");
        hideKeyboard();
        }
    private void refreshOnClick() {
        swipeRefreshLayout.setRefreshing(true);
        new GetProjectAsyncTask(MainActivity.this).execute(query);
        hideKeyboard();
    }

    private void hideKeyboard() {
        InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }

}
